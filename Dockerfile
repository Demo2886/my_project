FROM python:3

WORKDIR /code

COPY requirements.txt /code
RUN pip install -r requirements.txt
RUN mkdir db

COPY . /code
VOLUME /code
EXPOSE 8000
CMD sh init.sh && python manage.py runserver 0.0.0.0:8000




#FROM python:3
#ENV PYTHONUNBUFFERED 1
#RUN mkdir /code
#WORKDIR /code
#ADD . /code/
#RUN pip install -r requirements.txt
#CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000
